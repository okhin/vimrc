" Let's add some git signs
Plug 'airblade/vim-gitgutter'

" And use a git system
Plug 'jreybert/vimagit'
