set tabstop=4
set softtabstop=4
set shiftwidth=4
set textwidth=79
set expandtab
set autoindent
set fileformat=unix

" Virtualenvsupport for python
python3 << EOF
import os
import sys
if 'VIRTUAL_ENV' in os.environ:
    project_base_dir = os.environ['VIRTUAL_ENV']
    sys.path.insert(0, project_base_dir)
EOF

" And some hilights
let python_highlight_all=1
syntax on

" Some syntastic options
let g:syntastic_python_flake8_args = '--ignore E501,E128,E225'

" OmniCompletion
filetype plugin on
set omnifunc=pythoncomplete#Complete

" Run Syntastic on write
autocmd BufWrite *.py :Errors

" Folds
set foldmethod=indent
