" Racer is here
let g:racer_cmd = '/home/okhin/.cargo/bin/racer'
let g:racer_experimental_completer = 1

" Let's do some linting
let g:rustfmt_autosave = 1

" ctags are here
let g:rust_use_custom_ctags_defs = 1

" Let's define a cargo-test maker
let g:neomake_enabled_makers = ['cargotest']
let g:neomake_rust_enabled_makers = ['cargotest', 'cargo']

" Let's do some clever mapping for racer
nmap gd <Plug>(rust-def)
nmap gs <Plug>(rust-def-split)
nmap gv <Plug>(rust-def-vertical)
nmap K <Plug>(rust-doc)

" Let's define completion sources for rust
if executable("rls")
    au User lsp_setup call lsp#register_server({
        \ 'name': 'rls',
        \ 'priority': 50,
        \ 'cmd': { server_info->['rls']},
        \ 'whitelist': ['rust'],
    \ })
endif

" use racer as a complete soure
if executable('racer')
autocmd User asyncomplete_setup call asyncomplete#register_source(
            \ asyncomplete#sources#racer#get_source_options({
                \ 'priority': 50,
                \ 'whitelist': ['rust'],
                \ 'completor': function("asyncomplete#sources#racer#completor")
            \})
        \)
endif

" Let's get the project root marker for gutentags
let g:gutentags_project_root = ['Cargo.toml']

" Let use some project_info for gutentags
call add(g:gutentags_project_info, {'type': 'rust', 'file': 'Cargo.toml'})
